This ComfyUI workflow generates a Model Card which showcases the abilities of a Checkpoint or LoRA to generate varied categories of prompts.

A rewrite of the Model Card workflow by @Stevie2k8
See his posts: https://civitai.com/user/Stevie2k8

# Current Status
Issues: 
- Group Average gen times: check formula
- Global generation times: not working, issue with lists
- Wrong Headlines 24,25
- Mismatched prompts, negative prompts and labels.
- VRAM hungry

# Description

This workflow generates a customizable Model Card.

### Header

- Can be toggled on or off entirely.
- Logo to the left, toggable.
- Generation info.
- Baner to the right, toggable.

### Grid body

- Customizable number of columns (recommended values: 1 or 2).
- Toggable prompt groups (26).
- Customizable max number of images per group (recommended <8).
- Supports groups of different sizes.
- Each group prints one headline.
- Each group may print one description.
- Each image in group prints a label.

#### V3 exclusive
- Each image may print a generation time, toggable.
- (broken) Each group may print a total or average generation time.
- (planned) Total or global average generation time may print to the header.

# Use

- You may clone this repository into the `pysssss-workflows` ComfyUI subdirectory.
- Load the `.json` file.
- Install missing nodes using the ComfyUI Manager.
- (V3) Install my custom node "KSamplerTimer" for Version 3.
  - Install with the ComfyUI Manager: *Insall via git URL*: `https://gitlab.com/lanterieur/comfyui_ksamplertimer`
  - Get it from CivitAI: [CivitAI](https://civitai.com/models/396504/comfyui-ksampler-timer)
  - Get it from the repository: [Gitlab](https://gitlab.com/lanterieur/comfyui_ksamplertimer)

- Set checkpoint settings.
- Set default sd1.5 & sdxl image sizes.
- Set LoRA settings if needed.
- Set template (columns).
- Toggle groups to generate.
- (V2) Toggle "just the right amount" of "batch"es.
- (Recommended) Disable generation and dry fire.
- Queue workflow


# Changes

### V 3.0.1

- Fixed bugs

### V 3.0.0

- Complete refactor.
- Swapped native KSampler for custom KSamplerTimer.
- Supports batch generation with checkpoints configuration from file.
- Exposes negative prompts and seeds to user.
- Generates all images in one sampler run (list not batch). => Always regenerates all images.
- Adds ability to print generation times.
- Improved GUI: 
  - Toggles are black.
  - Anything Everywhere declarations are gold.
  - Number of column is red.
  - Save location controls moved to top.
  - Custom LoRA controls moved to top.
  - Reorganized variable columns.
- Fixed bugs

### V 2.0.0

- Refactor.
- Swapped multiple custom nodes (maths, simple & multiline strings).
- Improved GUI:
  - Reorganized nodes in columns by functions.
  - Color coded node groups.
  - Annotations for users.
- Removed save to jpg.